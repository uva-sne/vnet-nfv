#!/bin/sh
set -e
docker build -t skynet.lab.uvalight.net/$1 -f $1 .
if [ "$2" = "" ]; then
    docker push skynet.lab.uvalight.net/$1
else
    docker push skynet.lab.uvalight.net/$1:$2
fi
