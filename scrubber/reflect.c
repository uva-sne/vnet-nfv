/*
 * Origin:
 * http://stackoverflow.com/questions/14558125#answer-14588626
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <net/if.h>

//#include <linux/if_tun.h>

#define IFF_TAP 0x2
#define TUNSETIFF 0x400454ca


static int tun_alloc(char *dev)
{
    struct ifreq ifr;
    int fd, ret;

    if ((fd = open("/dev/net/tun", O_RDWR)) < 0) {
        perror("open");
        return -1;
    }

    memset(&ifr, 0, sizeof(ifr));

    ifr.ifr_flags = IFF_TAP;
    if (*dev)
        strncpy(ifr.ifr_name, dev, IFNAMSIZ);

    ret = ioctl(fd, TUNSETIFF, (void *)&ifr);
    if (ret < 0) {
        close(fd);
        perror("ioctl TUNSETIFF");
        return ret;
    }

    strcpy(dev, ifr.ifr_name);
    return fd;
}


static int iface_init(const char *dev)
{
    char buf[512];
    snprintf(buf, sizeof(buf) - 1,
             "ip addr flush dev %s; ip link set dev %s up", dev, dev);
    if (system(buf) < 0) {
        perror("system");
        return -1;
    }
    return 0;
}


int main(int argc, char **argv)
{
    int fd = -1;
    int ret = 1;
    char dev[IFNAMSIZ];

    if (argc < 2) {
        fprintf(stderr, "Usage: %s <interface>\n", argv[0]);
        return 1;
    }

    strncpy(dev, argv[1], IFNAMSIZ - 1);
    printf("Reflecting %s\n", dev);

    fd = tun_alloc(dev);
    if (fd < 0)
        goto out;

    if (iface_init(dev) < 0)
        goto out;

    while (1) {
        unsigned char packet[65535];
        int len = read(fd, packet, sizeof(packet));
        if (len < 0) {
            perror("read");
            goto out;
        }

        printf("incoming packet [%d octets]\n", len);

        len = write(fd, packet, len);
        printf("fed back packet [%d octets]\n", len);
    }

    ret = 0;

out:
    if (fd >= 0)
        close(fd);
    return ret;
}
