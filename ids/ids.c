#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>

#include <pcap.h>

#define SNAP_LEN 1518
#define ETHER_ADDR_LEN 6


struct sniff_ethernet {
    uint8_t  ether_dhost[ETHER_ADDR_LEN];
    uint8_t  ether_shost[ETHER_ADDR_LEN];
    uint16_t ether_type;
};

struct sniff_ethernet_vlan {
    uint8_t  ether_dhost[ETHER_ADDR_LEN];
    uint8_t  ether_shost[ETHER_ADDR_LEN];
    uint32_t ether_dot1q;
    uint16_t ether_type;
};


struct sniff_ip {
        uint8_t  ip_vhl;
        uint8_t  ip_tos;
        uint16_t ip_len;
        uint16_t ip_id;
        uint16_t ip_off;
        #define IP_RF 0x8000
        #define IP_DF 0x4000
        #define IP_MF 0x2000
        #define IP_OFFMASK 0x1fff
        uint8_t  ip_ttl;
        uint8_t  ip_p;
        uint16_t ip_sum;
        struct  in_addr ip_src, ip_dst;
};

#define IP_HL(ip) (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)  (((ip)->ip_vhl) >> 4)


struct sniff_tcp {
        uint16_t th_sport;
        uint16_t th_dport;
        uint32_t th_seq;
        uint32_t th_ack;
        uint8_t  th_offx2;
        uint8_t  th_flags;
        uint16_t th_win;
        uint16_t th_sum;
        uint16_t th_urp;
};

#define TH_OFF(th) (((th)->th_offx2 & 0xf0) >> 4)
#define TH_FIN  0x01
#define TH_SYN  0x02
#define TH_RST  0x04
#define TH_PUSH 0x08
#define TH_ACK  0x10
#define TH_URG  0x20
#define TH_ECE  0x40
#define TH_CWR  0x80
#define TH_FLAGS (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)


static const uint8_t *
strnstr(const uint8_t *haystack, size_t hlen,
        const uint8_t *needle, size_t nlen)
{
    if (!nlen)
        return haystack;

    for (size_t i = 0; i <= (hlen - nlen); i++) {
        if ((haystack[0] == needle[0]) &&
                (0 == strncmp((const char *) haystack, (const char * ) needle, nlen)))
            return haystack;
        haystack++;
    }

    return NULL;
}


static void
print_hex_ascii_line(const uint8_t *payload, int len, int offset)
{
    int i;
    int gap;
    const uint8_t *ch;

    /* offset */
    printf("%05d   ", offset);

    /* hex */
    ch = payload;
    for(i = 0; i < len; i++) {
        printf("%02x ", *ch);
        ch++;
        /* print extra space after 8th byte for visual aid */
        if (i == 7)
            printf(" ");
    }
    /* print space to handle line less than 8 bytes */
    if (len < 8)
        printf(" ");

    /* fill hex gap with spaces if not full line */
    if (len < 16) {
        gap = 16 - len;
        for (i = 0; i < gap; i++) {
            printf("   ");
        }
    }
    printf("   ");

    /* ascii (if printable) */
    ch = payload;
    for(i = 0; i < len; i++) {
        if (isprint(*ch))
            printf("%c", *ch);
        else
            printf(".");
        ch++;
    }

    printf("\n");
    return;
}


static void
print_payload(const uint8_t *payload, int len)
{
    int len_rem = len;
    int line_width = 16;
    int line_len;
    int offset = 0;
    const uint8_t *ch = payload;

    if (len <= 0)
        return;

    if (len <= line_width) {
        print_hex_ascii_line(ch, len, offset);
        return;
    }

    for (;;) {
        line_len = line_width % len_rem;
        print_hex_ascii_line(ch, line_len, offset);
        len_rem = len_rem - line_len;
        ch = ch + line_len;
        offset = offset + line_width;
        if (len_rem <= line_width) {
            print_hex_ascii_line(ch, len_rem, offset);
            break;
        }
    }

    return;
}

// --

static uint8_t atk_state[256*256] = {0};

#define BRUTEFORCE 1
#define CPUTHRESH 2

// Mark IP as being part of the attack
static void
store_bad_actor(const uint8_t *ip, int type)
{
    // XXX: we ignore the "10.100." prefix so we don't have to implement a
    //  more complex datastructure
    uint16_t id = (ip[2] << 8) | ip[3];
    uint8_t b = atk_state[id];
    if (!(b & type)) {
        printf("%d %d.%d.%d.%d\n", type, ip[0], ip[1], ip[2], ip[3]);
        atk_state[id] = b | type;
    }
}


static void
wipe_state()
{
    memset(atk_state, 0, 256*256);
}

// --


// TODO: verify OOB
static void
_ids_packet(uint8_t *args, const struct pcap_pkthdr *header, const uint8_t *packet)
{
    const struct sniff_ethernet *ethernet;
    const struct sniff_ip *ip;
    const struct sniff_tcp *tcp;
    const uint8_t *payload;
    uint16_t ether_type;
    size_t size_ethernet = 14;
    size_t size_ip;
    size_t size_tcp;
    size_t size_payload;

    (void) args;
    (void) header;

    ethernet = (struct sniff_ethernet*)(packet);
    ether_type = ntohs(ethernet->ether_type);
    if (ether_type == 0x8100) {
        const struct sniff_ethernet_vlan *ethernet_vlan;
        ethernet_vlan = (struct sniff_ethernet_vlan*)(packet);
        ether_type = ntohs(ethernet_vlan->ether_type);
        size_ethernet += 4;
    }

    if (ether_type != 0x0800)
        return;

    ip = (struct sniff_ip*)(packet + size_ethernet);
    size_ip = IP_HL(ip) * 4;
    if (size_ip < 20) {
        return;
    }

    // TODO: could detect UDP
    if (ip->ip_p != IPPROTO_TCP)
        return;

    tcp = (struct sniff_tcp *)(packet + size_ethernet + size_ip);
    size_tcp = TH_OFF(tcp) * 4;
    if (size_tcp < 20) {
        return;
    }

    if (ntohs(tcp->th_dport) != 80)
        return;

    payload = (uint8_t *)(packet + size_ethernet + size_ip + size_tcp);
    size_payload = ntohs(ip->ip_len) - (size_ip + size_tcp);

    if (size_payload <= 0)
        return;

    if (strnstr(payload, size_payload, (const uint8_t *) "GET /", 5) == NULL)
        return;

    const uint8_t *ip_src = (const uint8_t *) &ip->ip_src;
    const uint8_t *ptr;
    if ((ptr = strnstr(payload, size_payload, (const uint8_t *) "pw=", 3)) != NULL) {
        // TODO: should really consider all invalid password attempts and
        // threshold
        if (ptr[3] != 'N' && ptr[3] != 'Y')
            store_bad_actor(ip_src, BRUTEFORCE);
    } else if (strnstr(payload, size_payload, (const uint8_t *) "load=5012", 9) != NULL) {
        // TODO: should really store the number of times a "heavy" page was
        // accessed, and only mark clients that submit such requests frequently
        store_bad_actor(ip_src, CPUTHRESH);
    } else if (strnstr(payload, size_payload, (const uint8_t *) "load=9999", 9) != NULL) {
        store_bad_actor(ip_src, CPUTHRESH);
    }
}

static time_t last_state_time = 0;

static void
ids_packet(uint8_t *args, const struct pcap_pkthdr *header, const uint8_t *packet)
{
    _ids_packet(args, header, packet);

    // Every once in a while, flush the currently marked IPs
    //
    // TODO: doesn't execute if no packets arrive (i.e. when the IDS NFV is
    // removed from the network!)
    time_t now = header->ts.tv_sec;
    if (last_state_time == 0 || (now - last_state_time) > 1) {
        wipe_state();
        printf("reset\n");
        last_state_time = now;
    }
}


static sig_atomic_t done = 0;
static pcap_t *handle = NULL;

static void sigterm(int signum __attribute__((unused)))
{
    done = 1;

    if (handle)
        pcap_breakloop(handle);
}


int main(int argc, char **argv)
{
    char *dev = NULL;
    char errbuf[PCAP_ERRBUF_SIZE];

    if (argc == 2) {
        dev = argv[1];
    } else if (argc > 2) {
        fprintf(stderr, "error: unrecognized command-line options\n\n");
        exit(EXIT_FAILURE);
    } else {
        dev = pcap_lookupdev(errbuf);
        if (dev == NULL) {
            fprintf(stderr, "Couldn't find default device: %s\n",
                errbuf);
            exit(EXIT_FAILURE);
        }
    }

    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = sigterm;
    sigaction(SIGTERM, &action, NULL);

    setvbuf(stdout, NULL, _IOLBF, 0);

    while (!done) {
        handle = pcap_open_live(dev, SNAP_LEN, 1, 200, errbuf);
        if (handle == NULL) {
            fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
            goto pcap_error;
        }

        pcap_loop(handle, -1, ids_packet, NULL);

pcap_error:
        if (handle != NULL) {
            pcap_close(handle);
            handle = NULL;
        }

        if (!done)
            sleep(2);
    }


    return 0;
}
