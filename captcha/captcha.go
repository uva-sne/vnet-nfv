package main

import (
	"encoding/json"
	"io"
	"log"
	"net"
	"os"
	"sync"
	"syscall"
	//"time"

	"vnet.uvalight.net/utils/mqtt"
)

func publish(name string) {
	cli, err := mqtt.New(name)
	defer cli.Close()
	if err != nil {
		log.Fatalln("MQTT error:", err)
	}
	for {
		v, ok := <-cstats
		if !ok {
			break
		}
		cli.Pub("c/"+name+"/captcha", v)
	}
}

type H struct {
	stats chan string
}

type Stats struct {
	Success uint `json:"ok"`
	Fails   uint `json:"err"`
}

var (
	cstats  = make(chan string, 8)
	s       Stats
	counter uint
)

func trackStat(ok, err uint) {
	s.Success += ok
	s.Fails += err
	counter += 1
	if counter > 50 { // TIME!
		b, _ := json.Marshal(s)
		cstats <- string(b)
		s.Success = 0
		s.Fails = 0
		counter = 0
	}
}

func main() {
	name := os.Getenv("NAME")
	if name == "" {
		name = os.Getenv("HOSTNAME")
	}
	go publish(name)

	sock, err := net.Listen("tcp", ":3129")
	if err != nil {
		log.Fatal(err)
	}

	s := sock.(*net.TCPListener)
	sf, _ := s.File()
	fd := int(sf.Fd())
	err = syscall.SetsockoptInt(fd, syscall.SOL_IP, syscall.IP_TRANSPARENT, 1)
	//err = syscall.SetsockoptInt(fd, syscall.SOL_IP, syscall.IP_FREEBIND, 1)
	if err != nil {
		log.Fatalln("setsockopt:", err)
	}

	for {
		cli, err := s.Accept()
		if err != nil {
			log.Fatalln(err)
		}

		tcli := cli.(*net.TCPConn)

		addr, err := GetOriginalDST(tcli)
		if err != nil {
			log.Println("GetOriginalDST:", err)
			sock.Close()
			continue
		}

		go func(src *net.TCPConn, addr string) {
			defer src.Close()
			// TODO: read a bit, check if cookie header

			dst, err := DialOriginalDestination(src, false)
			if err != nil {
				log.Println("Dial error:", err)
				return
			}

			trackStat(1, 0)
			Pipe(src, dst)
		}(tcli, addr.String())
	}

	//server := http.Server{
	//	Handler: H{pws},
	//}
	//log.Fatal(server.Serve(sock))
}

var pool = sync.Pool{
	New: func() interface{} {
		return make([]byte, 64<<10)
	},
}

func Pipe(src, dest *net.TCPConn) {
	defer dest.Close()
	wg := &sync.WaitGroup{}
	wg.Add(2)

	go func() {
		defer wg.Done()
		buf := pool.Get().([]byte)
		io.CopyBuffer(dest, src, buf)
		pool.Put(buf)
		dest.CloseWrite()
		src.CloseRead()
	}()

	go func() {
		defer wg.Done()
		buf := pool.Get().([]byte)
		io.CopyBuffer(src, dest, buf)
		pool.Put(buf)
		src.CloseWrite()
		dest.CloseRead()
	}()

	wg.Wait()
}
