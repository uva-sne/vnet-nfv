.PHONY: push container
push: container
	docker push skynet.lab.uvalight.net/vnet-honeypot
	#docker push skynet.lab.uvalight.net/vnet-captcha
	#docker push skynet.lab.uvalight.net/vnet-squid-captcha
container:
	date > nocache
	#docker build -t skynet.lab.uvalight.net/vnet-captcha -f vnet-captcha .
	docker build -t skynet.lab.uvalight.net/vnet-honeypot -f vnet-honeypot .
	#(cd squid-captcha && docker build -t skynet.lab.uvalight.net/vnet-squid-captcha .)
