package main

import (
	"encoding/json"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"syscall"
	"time"

	"vnet.uvalight.net/utils/mqtt"
)

func publish(name string, pws chan string) {
	cli, err := mqtt.New(name)
	defer cli.Close()
	if err != nil {
		log.Fatalln("MQTT error:", err)
	}
	for {
		v, ok := <-pws
		if !ok {
			break
		}
		log.Println("Publish", v)
		if err := cli.Pub("c/"+name+"/pw", v); err != nil {
			log.Println("MQTT publish error:", err)
			break
		}
	}
	log.Fatalln("MQTT goodbye")
}

type H struct {
	pws   chan string
	track chan string
}

type Passwords struct {
	Passwords []string `json:"pws"`
	Counter   int      `json:"n"`
}

func (h H) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	rw.WriteHeader(403)
	q, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		log.Println(q, "Error:", err)
	}
	if q != nil {
		if v, ok := q["pw"]; ok {
			h.track <- v[0]
		}
	}
}

func (h H) publisher() {
	lastPass := make([]string, 3)
	lastIndex := 0
	counter := 0
	t := time.Tick(time.Second)
	for {
		select {
		case <-t:
			log.Println("Counter", counter)
			b, _ := json.Marshal(Passwords{lastPass, counter})
			h.pws <- string(b)
			counter = 0
		case v := <-h.track:
			lastPass[lastIndex] = v
			lastIndex = (lastIndex + 1) % 3
			counter += 1
		}
	}
}

func main() {
	name := os.Getenv("NAME")
	if name == "" {
		name = os.Getenv("HOSTNAME")
	}
	log.Println("I am", name)

	pws := make(chan string, 8)
	go publish(name, pws)

	sock, err := net.Listen("tcp", ":3129")
	if err != nil {
		log.Fatal(err)
	}

	s := sock.(*net.TCPListener)
	sf, _ := s.File()
	fd := int(sf.Fd())
	err = syscall.SetsockoptInt(fd, syscall.SOL_IP, syscall.IP_TRANSPARENT, 1)
	//err = syscall.SetsockoptInt(fd, syscall.SOL_IP, syscall.IP_FREEBIND, 1)
	if err != nil {
		log.Fatalln("setsockopt:", err)
	}

	h := H{pws, make(chan string, 16)}
	server := http.Server{
		Handler: h,
	}
	go h.publisher()
	log.Fatal(server.Serve(sock))
}
